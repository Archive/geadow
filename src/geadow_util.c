#include "geadow_util.h"

void
geadow_text_buffer_clear(GtkTextBuffer* buffer) {
  GtkTextIter start_iter;
  GtkTextIter end_iter;

  g_return_if_fail(buffer != NULL);
  g_return_if_fail(GTK_IS_TEXT_BUFFER(buffer));

  gtk_text_buffer_get_start_iter(buffer, &start_iter);
  gtk_text_buffer_get_end_iter(buffer, &end_iter);
  gtk_text_buffer_delete(buffer, &start_iter, &end_iter);
}
