#ifndef _GEADOW_INFO_H
#define _GEADOW_INFO_H

#include <gnome.h>
#include "geadow_buffer.h"
#include "geadow_view.h"

GeadowBuffer* geadow_info_get_current_buffer();
GeadowView* geadow_info_get_current_view();
void geadow_info_set_current_buffer(GeadowBuffer* buffer);


#endif /* _GEADOW_INFO_H */
