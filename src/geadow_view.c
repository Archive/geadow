#include "geadow_view.h"

typedef void (*GeadowViewFunc) (GeadowView* view);

typedef struct _GeadowKeyBind {
  gchar* key; /* C-x, M-v ... */
  GeadowViewFunc func;
} GeadowKeyBind;

#if 0
static void scroll_up(GeadowView* view) {}
static void scroll_down(GeadowView* view) {}
static void recenter(GeadowView* view) {}

GeadowKeyBind keybindtable[] = {
  { "C-v", scroll_up },
  { "M-v", scroll_down },
  { "C-l", recenter },
  { "M->", end_of_buffer },
  { "M-<", beginning_of_buffer },
  { "C-e", end_of_line },
  { "C-a", beginning_of_line },
  { "C-_", undo },
  { "C-/", undo },
  { NULL },
};
#endif

gboolean
on_geadow_view_key_press_event (GtkWidget* widget,
				GdkEventKey* event, gpointer user_data) {
  gint lines_in_textview;
  GtkTextIter iter;

  static gint prev_line_offset = 0;
  static gint metakey_modifier = 0;
  static gint controlx_modifier = 0;

  GtkTextView* textview = GTK_TEXT_VIEW(widget);
  GtkTextBuffer* textbuffer = gtk_text_view_get_buffer(textview);
  GtkIMMulticontext* textbuffer_imcontext;

  /* Calculate lines in textview */
  gtk_text_view_get_iter_at_location(textview, &iter, 0,
				     widget->allocation.height);
  lines_in_textview = gtk_text_iter_get_line(&iter);

  //g_print("state: %d, keyval: 0x%02x\n", event->state, event->keyval);

  textbuffer_imcontext = GTK_IM_MULTICONTEXT(textview->im_context);

  /* If immodule is Japanese input mode, it should pass all mnemonic. */
  if( g_object_get_data(G_OBJECT(textbuffer_imcontext->slave),
    "immodule-needs-mnemonic") )
    return FALSE;

  if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_g ) {
    /* Reset Modifier */
    metakey_modifier = 0;
    controlx_modifier = 0;
    gtk_widget_grab_focus(widget);
    return TRUE;
  }

  if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_a ) {
    gtk_text_buffer_get_iter_at_mark(textbuffer, &iter,
				     gtk_text_buffer_get_insert(textbuffer));
    gtk_text_iter_set_line_offset(&iter, 0);
    gtk_text_buffer_place_cursor(textbuffer, &iter);
    prev_line_offset = 0;
    return TRUE;
  } else if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_e ) {
    gtk_text_buffer_get_iter_at_mark(textbuffer, &iter,
				     gtk_text_buffer_get_insert(textbuffer));
    if( gtk_text_iter_get_char(&iter) != 0x0a ) {
      gtk_text_iter_forward_to_line_end(&iter);
    }
    gtk_text_buffer_place_cursor(textbuffer, &iter);
    prev_line_offset = 0;
    return TRUE;
  } else if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_p ) {
    gint current_offset;
    gint chars_in_line;
    gtk_text_buffer_get_iter_at_mark(textbuffer, &iter,
				     gtk_text_buffer_get_insert(textbuffer));
    current_offset = gtk_text_iter_get_line_offset(&iter);
    prev_line_offset = MAX(current_offset, prev_line_offset);
    gtk_text_iter_backward_line(&iter);
    chars_in_line = gtk_text_iter_get_chars_in_line(&iter);
    chars_in_line -= 1;
    if( chars_in_line < 0 )
      chars_in_line = 0;
    gtk_text_iter_set_line_offset(&iter, MIN(chars_in_line,prev_line_offset));
    gtk_text_buffer_place_cursor(textbuffer, &iter);
    if( gtk_text_view_place_cursor_onscreen(textview) ) {
      gtk_text_iter_forward_lines(&iter, -lines_in_textview/2);
      gtk_text_view_scroll_to_iter(textview, &iter, 0, FALSE, 0, 0);
    }
    return TRUE;
  } else if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_n ) {
    gint current_offset;
    gint chars_in_line;
    gtk_text_buffer_get_iter_at_mark(textbuffer, &iter,
				     gtk_text_buffer_get_insert(textbuffer));
    current_offset = gtk_text_iter_get_line_offset(&iter);
    prev_line_offset = MAX(current_offset, prev_line_offset);
    gtk_text_iter_forward_line(&iter);
    chars_in_line = gtk_text_iter_get_chars_in_line(&iter);
    chars_in_line -= 1;
    if( chars_in_line < 0 )
      chars_in_line = 0;
    gtk_text_iter_set_line_offset(&iter, MIN(chars_in_line,prev_line_offset));
    gtk_text_buffer_place_cursor(textbuffer, &iter);
    if( gtk_text_view_place_cursor_onscreen(textview) ) {
      gtk_text_iter_forward_lines(&iter, lines_in_textview/2);
      gtk_text_view_scroll_to_iter(textview, &iter, 0, FALSE, 0, 0);
    }
    return TRUE;
  } else if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_b ) {
    gtk_text_buffer_get_iter_at_mark(textbuffer, &iter,
				     gtk_text_buffer_get_insert(textbuffer));
    gtk_text_iter_backward_char(&iter);
    gtk_text_buffer_place_cursor(textbuffer, &iter);
    gtk_text_view_scroll_mark_onscreen(textview,
				       gtk_text_buffer_get_insert(textbuffer));
    prev_line_offset = 0;
    return TRUE;
  } else if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_f) {
    gtk_text_buffer_get_iter_at_mark(textbuffer, &iter,
				     gtk_text_buffer_get_insert(textbuffer));
    gtk_text_iter_forward_char(&iter);
    gtk_text_buffer_place_cursor(textbuffer, &iter);
    gtk_text_view_scroll_mark_onscreen(textview,
				       gtk_text_buffer_get_insert(textbuffer));
    prev_line_offset = 0;
    return TRUE;
  }

  return FALSE;
}

GeadowView*
geadow_view_new() {
  GeadowView* view = g_new0(GeadowView, 1);

  GtkLabel* modelabel;
  GtkScrolledWindow* scrolledwindow;
  
  view->textview = GTK_TEXT_VIEW(gtk_text_view_new());
  view->scrolledwindow = gtk_scrolled_window_new(NULL, NULL);
  view->vbox = gtk_vbox_new(FALSE, 0);
  view->modelabel = gtk_label_new("-E:-- *scratch* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

  scrolledwindow = GTK_SCROLLED_WINDOW(view->scrolledwindow);
  modelabel = GTK_LABEL(view->modelabel);

  gtk_box_pack_start(GTK_BOX(view->vbox), view->scrolledwindow, TRUE, TRUE, 0);
  gtk_scrolled_window_set_policy(scrolledwindow, GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type(scrolledwindow, GTK_SHADOW_ETCHED_IN);

  gtk_container_add(GTK_CONTAINER(scrolledwindow), GTK_WIDGET(view->textview));
  gtk_box_pack_start(GTK_BOX(view->vbox), view->modelabel, FALSE, FALSE, 0);
  gtk_label_set_justify(modelabel, GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment(GTK_MISC(modelabel), 0, 0.5);

  gtk_widget_show_all(view->vbox);
  geadow_view_update_modelabel(view);

  g_signal_connect(G_OBJECT(view->textview), "key_press_event",
		   G_CALLBACK(on_geadow_view_key_press_event), view);

  return view;
}

static void
on_textbuffer_mark_set(GtkTextBuffer *buffer, const GtkTextIter *iter,
		       GtkTextMark* mark, gpointer data) {
  GeadowView* view = (GeadowView*)data;

  geadow_view_update_modelabel(view);
}

void
geadow_view_set_buffer(GeadowView* view, GeadowBuffer* buffer) {
  g_return_if_fail(view != NULL);
  g_return_if_fail(buffer != NULL);

  if( view->buffer != NULL ) {
    g_signal_handlers_disconnect_by_func(view->buffer->textbuffer,
				G_CALLBACK(on_textbuffer_mark_set), view);
  }

  view->buffer = buffer;
  gtk_text_view_set_buffer(view->textview, buffer->textbuffer);

  geadow_view_update_modelabel(view);

  if( buffer != NULL && GTK_IS_TEXT_BUFFER(buffer->textbuffer) ) {
    g_signal_connect(buffer->textbuffer, "mark_set",
		     G_CALLBACK(on_textbuffer_mark_set),
		     view);
  }
}

void
geadow_view_update_modelabel(GeadowView* view) {
  gchar* labeltext;
  gchar* buffername;
  gboolean modified;
  GtkTextIter iter;
  gint linenum = 1;

  g_return_if_fail(view != NULL);

  buffername = !view->buffer ? "" : !view->buffer->buffername ? "" : view->buffer->buffername;
  modified = !view->buffer ? FALSE : gtk_text_buffer_get_modified(view->buffer->textbuffer);
  if( view->buffer ) {
    gtk_text_buffer_get_iter_at_mark(view->buffer->textbuffer, &iter, gtk_text_buffer_get_insert(view->buffer->textbuffer));
    linenum = gtk_text_iter_get_line(&iter) + 1;
  }

  labeltext = g_strdup_printf("-E:%s %-16s --------------------------------L%d-----------------------------------------------------------------------------------------------------------------------------------------------------------",
        modified ? "**" : "--", 
        buffername, linenum);

  gtk_label_set_text(GTK_LABEL(view->modelabel), labeltext);
  g_free(labeltext);
}

void
geadow_view_grab_focus(GeadowView* view) {
  g_return_if_fail(view != NULL);

  gtk_widget_grab_focus(GTK_WIDGET(view->textview));
}

void
geadow_view_cut_clipboard(GeadowView* view) {
  GtkTextView* textview = view->textview;
  GtkTextBuffer* textbuffer = view->buffer->textbuffer;
  GtkClipboard *clipboard =
    gtk_widget_get_clipboard (GTK_WIDGET(view->textview),
			      GDK_SELECTION_CLIPBOARD);

  gtk_text_buffer_cut_clipboard (textbuffer,
                                 clipboard, textview->editable);
  gtk_text_view_scroll_mark_onscreen (textview,
                                      gtk_text_buffer_get_mark (textbuffer,
                                                                "insert"));

}

void
geadow_view_copy_clipboard(GeadowView* view) {
  GtkTextView* textview = view->textview;
  GtkTextBuffer* textbuffer = view->buffer->textbuffer;
  GtkClipboard *clipboard =
    gtk_widget_get_clipboard (GTK_WIDGET(view->textview),
			      GDK_SELECTION_CLIPBOARD);

  gtk_text_buffer_copy_clipboard (textbuffer,
                                 clipboard);
  gtk_text_view_scroll_mark_onscreen (textview,
                                      gtk_text_buffer_get_mark (textbuffer,
                                                                "insert"));
}

void
geadow_view_paste_clipboard(GeadowView* view) {
  GtkTextView* textview = view->textview;
  GtkTextBuffer* textbuffer = view->buffer->textbuffer;
  GtkClipboard *clipboard =
    gtk_widget_get_clipboard (GTK_WIDGET(view->textview),
			      GDK_SELECTION_CLIPBOARD);

  gtk_text_buffer_paste_clipboard (textbuffer,
                                 clipboard, NULL, textview->editable);
  gtk_text_view_scroll_mark_onscreen (textview,
                                      gtk_text_buffer_get_mark (textbuffer,
                                                                "insert"));
}

void
geadow_view_delete_selection(GeadowView* view) {
  GtkTextView* textview = view->textview;
  GtkTextBuffer* textbuffer = view->buffer->textbuffer;

  gtk_text_buffer_delete_selection(textbuffer, FALSE, textview->editable);

  gtk_text_view_scroll_mark_onscreen (textview,
                                      gtk_text_buffer_get_mark (textbuffer,
                                                                "insert"));
}
