#ifndef _MINIBUF_H
#define _MINIBUF_H

#include <gnome.h>
#include <glade/glade.h>

#include "geadow_util.h"

extern GladeXML* xml;

void geadow_minibuf_clear();


#endif /* _MINIBUF_H */
