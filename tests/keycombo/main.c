#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

GtkWidget* label = NULL;
GSList* ekc_slist = NULL; /* Emacs Key Combo */

void
setlabel(gchar* text) {
  if( label ) {
    if( text == NULL || *text == '\0' ) {
      gtk_label_set_text(GTK_LABEL(label), "[INPUT EMACS KEY]");
      return;
    }
    gtk_label_set_text(GTK_LABEL(label), text);
  }
}

void
ekc_init() {
  gint i;

  if( ekc_slist ) {
    for(i=0;i<g_slist_length(ekc_slist);i++) {
      gchar* text = g_slist_nth_data(ekc_slist, i);
      g_free(text);
    }
    g_slist_free(ekc_slist);
  }
  ekc_slist = NULL;
  setlabel(NULL);
}

gchar*
get_ekc() {
  guint i;
  gchar* result = NULL;
  gchar** tmpbuf;

  if( ekc_slist == NULL || g_slist_length(ekc_slist) == 0 )
    return NULL;

  tmpbuf = g_new0(gchar*, g_slist_length(ekc_slist)+1);
  for(i=0;i<g_slist_length(ekc_slist);i++) {
    tmpbuf[i] = (gchar*)g_slist_nth_data(ekc_slist, i);
  }
  result = g_strjoinv(" ", tmpbuf);

  return result;
}

gboolean
ekc_add(GdkEventKey* event) {
  gchar* addtext = NULL;

  if( event->state & GDK_CONTROL_MASK ) {
    gunichar uchar = gdk_keyval_to_unicode(event->keyval);
    gchar* tmpbuf = g_new0(gchar, 7);
    g_unichar_to_utf8(uchar, tmpbuf);
    if( strlen(gdk_keyval_name(event->keyval)) != 1 ) {
      g_free(tmpbuf);
      return FALSE;
    }
    addtext = g_strconcat("C-", tmpbuf, NULL);
    g_free(tmpbuf);
  } else if( event->state & GDK_MOD1_MASK ) {
    gunichar uchar = gdk_keyval_to_unicode(event->keyval);
    gchar* tmpbuf = g_new0(gchar, 7);
    g_unichar_to_utf8(uchar, tmpbuf);
    if( strlen(gdk_keyval_name(event->keyval)) != 1 ) {
      g_free(tmpbuf);
      return FALSE;
    }
    addtext = g_strconcat("M-", tmpbuf, NULL);
    g_free(tmpbuf);
  }

  if( addtext && !strcmp(addtext, "C-g") ) {
    g_free(addtext);
    ekc_init();
    return TRUE;
  }

  if( addtext ) {
    ekc_slist = g_slist_append(ekc_slist, addtext);
    return TRUE;
  }

  return FALSE;
}

void
on_key_press_event(GtkWidget* widget, GdkEventKey* event, gpointer data) {
  if( event->keyval == GDK_Escape ) {
    gtk_main_quit();
    return;
  }

  ekc_add(event);
  if( ekc_slist ) {
    gchar* ekctext = get_ekc();
    if( ekctext ) {
      setlabel(ekctext);
      g_free(ekctext);
    }
  }
}

int main(int argc, char** argv) {
  GtkWidget* window;

  gtk_set_locale();
  gtk_init(&argc, &argv);

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_default_size(GTK_WINDOW(window), 600, 400);
  g_signal_connect(G_OBJECT(window), "key_press_event",
                   G_CALLBACK(on_key_press_event), window);

  label = gtk_label_new("[INPUT EMACS KEY]");
  gtk_container_add(GTK_CONTAINER(window), label);

  gtk_widget_show_all(window);

  gtk_main();

  return 0;
}
