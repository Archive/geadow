#include <glade/glade.h>
#include <gnome.h>

#include "geadow_buffer.h"
#include "geadow_view.h"
#include "geadow_util.h"
#include "minibuf.h"

#include "geadow_info.h"

#define PACKAGE "geadow"
#define VERSION "0.1.1"

typedef struct _GeadowInfo {
  GeadowBuffer* current_buffer;
  GeadowBuffer* scratch_buffer;
  GeadowView* current_view;
  GSList* geadow_buffer_slist;
  GSList* geadow_view_slist;

  GtkWidget* buffer_menu;
} GeadowInfo;

static GnomeProgram *program = NULL;
GtkWidget* buffer_menu = NULL;
GladeXML* xml = NULL;
static GeadowInfo* geadow_info = NULL;
static gnu_startup_timeout_handler_id = -1;

static void geadow_quit();
void on_geadow_open_activate(GtkMenuItem *menuitem, gpointer user_data);
void on_geadow_save_activate(GtkMenuItem *menuitem, gpointer user_data);
void on_about1_activate (GtkMenuItem *menuitem, gpointer user_data);
void gnu_startup_end();

void on_geadow_cut_activate (GtkMenuItem *menuitem, gpointer user_data);
void on_geadow_copy_activate (GtkMenuItem *menuitem, gpointer user_data);
void on_geadow_paste_activate (GtkMenuItem *menuitem, gpointer user_data);
void on_geadow_clear_activate (GtkMenuItem *menuitem, gpointer user_data);

static GnomeUIInfo file_menu_uiinfo[] = {
  { GNOME_APP_UI_ITEM, N_("_Open..."),
    N_("Open a file"),
    on_geadow_open_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GTK_STOCK_OPEN, 0, 0 },
  { GNOME_APP_UI_ITEM, N_("_Save"),
    N_("Save the current file"),
    on_geadow_save_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GTK_STOCK_SAVE, 0, 0 },
  { GNOME_APP_UI_ITEM, N_("Save _As..."),
    N_("Save the current file with a different name"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GTK_STOCK_SAVE_AS, 0, 0 },
  GNOMEUIINFO_SEPARATOR,
  { GNOME_APP_UI_ITEM, N_("_Quit"),
    N_("Quit the application"),
    geadow_quit, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GTK_STOCK_QUIT, 0, 0 },
  GNOMEUIINFO_END
};

static GnomeUIInfo edit_menu_uiinfo[] = {
  { GNOME_APP_UI_ITEM, N_("_Undo"),
    N_("Undo the last action"),
    geadow_quit, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GTK_STOCK_UNDO, 0, 0 },
  { GNOME_APP_UI_ITEM, N_("_Redo"),
    N_("Redo the undone action"),
    geadow_quit, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GTK_STOCK_REDO, 0, 0 },
  GNOMEUIINFO_SEPARATOR,
  { GNOME_APP_UI_ITEM, N_("Cu_t"),
    N_("Cut the selection"),
    on_geadow_cut_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GTK_STOCK_CUT, 0, 0 },
  { GNOME_APP_UI_ITEM, N_("_Copy"),
    N_("Copy the selection"),
    on_geadow_copy_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GTK_STOCK_COPY, 0, 0 },
  { GNOME_APP_UI_ITEM, N_("_Paste"),
    N_("Paste the clipboard"),
    on_geadow_paste_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GTK_STOCK_PASTE, 0, 0 },
  { GNOME_APP_UI_ITEM, N_("C_lear"),
    N_("Clear the selection"),
    on_geadow_clear_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GTK_STOCK_DELETE, 0, 0 },
  GNOMEUIINFO_END
};

static GnomeUIInfo view_menu_uiinfo[] = {
  GNOMEUIINFO_END
};

static GnomeUIInfo buffer_menu_uiinfo[] = {
  GNOMEUIINFO_END
};

static GnomeUIInfo help_menu_uiinfo[] = {
  { GNOME_APP_UI_ITEM, N_("_About"),
    N_("About this application"),
    on_about1_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_ABOUT, 0, 0 },
  GNOMEUIINFO_END
};

static GnomeUIInfo menubar_uiinfo[] = {
  GNOMEUIINFO_MENU_FILE_TREE(file_menu_uiinfo),
  GNOMEUIINFO_MENU_EDIT_TREE(edit_menu_uiinfo),
  GNOMEUIINFO_MENU_VIEW_TREE(view_menu_uiinfo),
  { GNOME_APP_UI_SUBTREE, N_("_Buffer"), NULL, buffer_menu_uiinfo,
    NULL, NULL, (GnomeUIPixmapType) 0, NULL, 0,
    (GdkModifierType) 0, NULL },
  GNOMEUIINFO_MENU_HELP_TREE(help_menu_uiinfo),
  GNOMEUIINFO_END
};

void
geadow_open_file(const gchar* filepath) {
  GeadowBuffer* newbuffer;

  g_return_if_fail(filepath != NULL);
  g_return_if_fail(*filepath != '\0');

  newbuffer = geadow_buffer_new();
  geadow_buffer_open_file(newbuffer, (gchar*)filepath);
  geadow_view_set_buffer(geadow_info->current_view, newbuffer);
  geadow_info->current_buffer = newbuffer;
  geadow_info->geadow_buffer_slist =
	g_slist_append(geadow_info->geadow_buffer_slist, newbuffer);
  geadow_view_grab_focus(geadow_info->current_view);
}

void
geadow_save_file() {
  GeadowBuffer* buffer = geadow_info->current_buffer;

  geadow_buffer_save_file(buffer);
  geadow_view_update_modelabel(geadow_info->current_view);
}

static void
geadow_quit() {
  gtk_main_quit();
}

void
on_geadow_open_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data) {
  GtkWidget* open_fileselection = glade_xml_get_widget(xml, "open_fileselection");

  if( gnu_startup_timeout_handler_id != -1 )
    return;

  gtk_widget_show_all(open_fileselection);
}

void
on_geadow_save_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data) {
  geadow_save_file();
}

void
on_open_fileselection_destroy(GtkWidget* widget, gpointer user_data) {
  return;
}

void
on_open_fileselection_ok_button_clicked (GtkButton       *button,
                                        gpointer         user_data) {
  GtkWidget* open_fileselection = glade_xml_get_widget(xml, "open_fileselection");
  gtk_widget_hide_all(open_fileselection);

  geadow_open_file(gtk_file_selection_get_filename(GTK_FILE_SELECTION(open_fileselection)));
}


void
on_open_fileselection_cancel_button_clicked (GtkButton       *button,
                                        gpointer         user_data) {
  GtkWidget* open_fileselection = glade_xml_get_widget(xml, "open_fileselection");
  gtk_widget_hide_all(open_fileselection);
}

void
on_about1_activate (GtkMenuItem *menuitem, gpointer user_data) {
  GtkWidget* geadow_about = NULL;

  const gchar *authors[] = {
    _("Yukihiro Nakai <ynakai@redhat.com>"),
    NULL
  };
  const gchar *documenters[] = { NULL };

  geadow_about = gnome_about_new(PACKAGE, VERSION,
				_("Copyright (C) 2003 Yukihiro Nakai"),
				_("GNU Geadow"),
				authors,
				documenters,
				_("translator_credits"),
				NULL);

  gtk_widget_show_all(geadow_about);
}

void
on_geadow_cut_activate (GtkMenuItem *menuitem, gpointer user_data) {
  geadow_view_cut_clipboard(geadow_info->current_view);
}

void
on_geadow_copy_activate (GtkMenuItem *menuitem, gpointer user_data) {
  geadow_view_copy_clipboard(geadow_info->current_view);
}

void
on_geadow_paste_activate (GtkMenuItem *menuitem, gpointer user_data) {
  geadow_view_paste_clipboard(geadow_info->current_view);
}

void
on_geadow_clear_activate (GtkMenuItem *menuitem, gpointer user_data) {
  geadow_view_delete_selection(geadow_info->current_view);
}


void
on_geadow_quit_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data) {
  geadow_quit();
}

void
on_geadow_app_destroy                  (GtkObject       *object,
                                        gpointer         user_data) {
  geadow_quit();
}

void
on_geadow_open_button_clicked          (GtkButton       *button,
                                        gpointer         user_data) {
  GtkWidget* open_fileselection = glade_xml_get_widget(xml, "open_fileselection");

  if( gnu_startup_timeout_handler_id != -1 )
    return;

  gtk_widget_show_all(open_fileselection);
}

void
on_geadow_save_button_clicked(GtkButton* button, gpointer user_data) {
  geadow_save_file();
}

gboolean
on_buffer_textview_key_press_event     (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data) {
#if 0
  GtkTextIter iter;
  GtkWidget* buffer_textview = glade_xml_get_widget(xml, "buffer_textview");
  GtkTextBuffer* textbuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(buffer_textview));
  static gint prev_line_offset = 0;
  static gint metakey_modifier = 0;
  static gint controlx_modifier = 0;

  /* Finish Emacs startup */
  if( gnu_startup_timeout_handler_id != -1 ) {
    gnu_startup_end();
    return FALSE;
  }

  /* Calculate lines in textview */
  gtk_text_view_get_iter_at_location(GTK_TEXT_VIEW(buffer_textview),
				     &iter, 0,
				     buffer_textview->allocation.height);
  geadow_info->lines_in_textview = gtk_text_iter_get_line(&iter);

//g_print("state: %d, keyval: 0x%02x\n", event->state, event->keyval);

  /* TEST */
  if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_t ) {
    GtkWidget* vbox1 = glade_xml_get_widget(xml, "geadow_app_vbox");

    static GeadowBuffer* buffer = NULL;

    GeadowView* view = geadow_view_new();

    if( buffer == NULL ) {
       buffer = geadow_buffer_new();
       geadow_buffer_open_file(buffer, "/tmp/test.gl");
    }
    geadow_view_set_buffer(view, buffer);

    gtk_box_pack_start(GTK_BOX(vbox1), view->vbox, TRUE, TRUE, 0);
    gtk_box_reorder_child(GTK_BOX(vbox1), view->vbox,  1);
    gtk_widget_show_all(vbox1);
    return TRUE;
  }

  /* C-g */
  if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_g ) {
    /* Reset Modifier */
    metakey_modifier = 0;
    controlx_modifier = 0;
    gtk_widget_grab_focus(buffer_textview);
    return TRUE;
  }

  /* C-x */
  if( controlx_modifier >= 1 ) {
    if( metakey_modifier >= 1 )
      return TRUE;

    if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_s ) {
      geadow_save_buffer();
      controlx_modifier = 0;
      return TRUE;
    } else if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_c ) {
      controlx_modifier = 0;
      geadow_quit();
      return TRUE;
    }

    return TRUE;
  }

  /* ESC- */
  if( metakey_modifier >= 1 ) {
    if( event->keyval == GDK_greater ) {
      GtkTextIter iter;
      gtk_text_buffer_get_end_iter(textbuffer, &iter);
      gtk_text_buffer_place_cursor(textbuffer, &iter);
      gtk_text_view_scroll_mark_onscreen(GTK_TEXT_VIEW(buffer_textview), gtk_text_buffer_get_insert(textbuffer));
      metakey_modifier = 0;
      return TRUE;
    } else if ( event->keyval == GDK_less ) {
      GtkTextIter iter;
      gtk_text_buffer_get_start_iter(textbuffer, &iter);
      gtk_text_buffer_place_cursor(textbuffer, &iter);
      gtk_text_view_scroll_mark_onscreen(GTK_TEXT_VIEW(buffer_textview), gtk_text_buffer_get_insert(textbuffer));
      return TRUE;
    }

    return TRUE;
  }

  if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_s ) {
    geadow_minibuf_set_modetext("I-Search: ");
    g_print("C-s\n");
    return TRUE;
  } else if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_v ) {
    GtkTextIter iter;
    gtk_text_buffer_get_iter_at_mark(textbuffer, &iter,
				     gtk_text_buffer_get_insert(textbuffer));
    gtk_text_iter_forward_lines(&iter, geadow_info->lines_in_textview);
    gtk_text_buffer_place_cursor(textbuffer, &iter);
    gtk_text_view_scroll_to_iter(GTK_TEXT_VIEW(buffer_textview), &iter, 0, FALSE, 0, 0);
    g_print("C-v\n");
    return TRUE;
  } else if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_a ) {
    GtkTextIter iter;
    gtk_text_buffer_get_iter_at_mark(textbuffer, &iter,
				     gtk_text_buffer_get_insert(textbuffer));
    gtk_text_iter_set_line_offset(&iter, 0);
    gtk_text_buffer_place_cursor(textbuffer, &iter);
    prev_line_offset = 0;
    return TRUE;
  } else if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_e ) {
    GtkTextIter iter;
    gint chars_in_line;
    gtk_text_buffer_get_iter_at_mark(textbuffer, &iter,
				     gtk_text_buffer_get_insert(textbuffer));
    if( gtk_text_iter_get_char(&iter) != 0x0a ) {
      gtk_text_iter_forward_to_line_end(&iter);
    }
    gtk_text_buffer_place_cursor(textbuffer, &iter);

    prev_line_offset = 0;
    return TRUE;
  } else if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_p ) {
    GtkTextIter iter;
    gint current_offset;
    gint chars_in_line;
    gtk_text_buffer_get_iter_at_mark(textbuffer, &iter,
				     gtk_text_buffer_get_insert(textbuffer));
    current_offset = gtk_text_iter_get_line_offset(&iter);
    prev_line_offset = MAX(current_offset, prev_line_offset);
    gtk_text_iter_backward_line(&iter);
    chars_in_line = gtk_text_iter_get_chars_in_line(&iter);
    chars_in_line -= 1;
    if( chars_in_line < 0 )
      chars_in_line = 0;
    gtk_text_iter_set_line_offset(&iter, MIN(chars_in_line,prev_line_offset));
    gtk_text_buffer_place_cursor(textbuffer, &iter);
    if( gtk_text_view_place_cursor_onscreen(GTK_TEXT_VIEW(buffer_textview)) ) {
      gtk_text_iter_forward_lines(&iter, -geadow_info->lines_in_textview/2);
      gtk_text_view_scroll_to_iter(GTK_TEXT_VIEW(buffer_textview), &iter, 0, FALSE, 0, 0);
    }
    return TRUE;
  } else if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_n ) {
    GtkTextIter iter;
    gint current_offset;
    gint chars_in_line;
    gtk_text_buffer_get_iter_at_mark(textbuffer, &iter,
				     gtk_text_buffer_get_insert(textbuffer));
    current_offset = gtk_text_iter_get_line_offset(&iter);
    prev_line_offset = MAX(current_offset, prev_line_offset);
    gtk_text_iter_forward_line(&iter);
    chars_in_line = gtk_text_iter_get_chars_in_line(&iter);
    chars_in_line -= 1;
    if( chars_in_line < 0 )
      chars_in_line = 0;
    gtk_text_iter_set_line_offset(&iter, MIN(chars_in_line,prev_line_offset));
    gtk_text_buffer_place_cursor(textbuffer, &iter);
    if( gtk_text_view_place_cursor_onscreen(GTK_TEXT_VIEW(buffer_textview)) ) {
      gtk_text_iter_forward_lines(&iter, (int)(geadow_info->lines_in_textview/2));
      gtk_text_view_scroll_to_iter(GTK_TEXT_VIEW(buffer_textview), &iter, 0, FALSE, 0, 0);
    }
    return TRUE;
  } else if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_b ) {
    GtkTextIter iter;
    gtk_text_buffer_get_iter_at_mark(textbuffer, &iter,
				     gtk_text_buffer_get_insert(textbuffer));
    gtk_text_iter_backward_char(&iter);
    gtk_text_buffer_place_cursor(textbuffer, &iter);
    gtk_text_view_scroll_mark_onscreen(GTK_TEXT_VIEW(buffer_textview), gtk_text_buffer_get_insert(textbuffer));
    prev_line_offset = 0;
    return TRUE;
  } else if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_f ) {
    GtkTextIter iter;
    gtk_text_buffer_get_iter_at_mark(textbuffer, &iter,
				     gtk_text_buffer_get_insert(textbuffer));
    gtk_text_iter_forward_char(&iter);
    gtk_text_buffer_place_cursor(textbuffer, &iter);
    gtk_text_view_scroll_mark_onscreen(GTK_TEXT_VIEW(buffer_textview), gtk_text_buffer_get_insert(textbuffer));
    prev_line_offset = 0;
    return TRUE;
  }

  if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_x ) {
    controlx_modifier = 1;
    return TRUE;
  }

  switch(event->keyval) {
  case GDK_Escape:
    g_print("ESC\n");
    metakey_modifier = 1;
    return TRUE;
    break;
  default:
    break;
  }
#endif
  return FALSE;
}

void
gnu_startup_end() {
  GtkTextView* textview = geadow_info->current_view->textview;
  GtkTextBuffer* textbuffer = geadow_info->current_buffer->textbuffer;

  if( gnu_startup_timeout_handler_id != -1 )
    gtk_timeout_remove(gnu_startup_timeout_handler_id);
  gnu_startup_timeout_handler_id = -1;

  g_signal_handlers_disconnect_by_func(textview,
				      G_CALLBACK(gnu_startup_end), NULL);


  gtk_text_view_set_editable(textview, TRUE);
  gtk_text_view_set_cursor_visible(textview, TRUE);
  gtk_widget_grab_focus(GTK_WIDGET(textview));
  geadow_view_set_buffer(geadow_info->current_view,
			 geadow_info->scratch_buffer);
  geadow_info->current_buffer = geadow_info->scratch_buffer;
}

void
gnu_startup() {
  GError *error = NULL;
  GdkPixbuf* gnupixbuf = gdk_pixbuf_new_from_file("/usr/share/emacs/21.3/etc/splash.xpm", &error); 

  GeadowBuffer* splash_buffer = geadow_buffer_new();
  GtkTextBuffer* textbuffer = splash_buffer->textbuffer;
  GtkTextView* textview = geadow_info->current_view->textview;
  GtkTextIter iter;

  geadow_buffer_set_buffername(splash_buffer, "Geadow");
  geadow_view_set_buffer(geadow_info->current_view, splash_buffer);
  geadow_info->current_buffer = splash_buffer;

  gtk_text_buffer_create_tag(textbuffer, "redfont", "foreground", "red", NULL);
  gtk_text_buffer_create_tag(textbuffer, "bigfont", "size", 12*PANGO_SCALE, NULL);
  gtk_text_buffer_create_tag(textbuffer, "bold", "weight", PANGO_WEIGHT_HEAVY, NULL);

  gtk_text_buffer_get_start_iter(textbuffer, &iter);

  gtk_text_iter_forward_chars(&iter, 10);
  gtk_text_buffer_insert(textbuffer, &iter, "                    ", -1);
  if( gnupixbuf )
    gtk_text_buffer_insert_pixbuf(textbuffer, &iter, gnupixbuf);
  gtk_text_buffer_insert(textbuffer, &iter, "\n\n", -1);
  gtk_text_buffer_insert_with_tags_by_name(textbuffer, &iter, _("Geadow is one component of Emacs-like editor for GNOME2\n"), -1, "redfont", "bigfont", NULL);
  gtk_text_buffer_insert_with_tags_by_name(textbuffer, &iter, _("You can do basic editing with the menu bar and scroll bar using the mouse.\n"), -1, "bigfont", NULL);
  gtk_text_buffer_insert(textbuffer, &iter, "\n", -1);
  gtk_text_buffer_insert_with_tags_by_name(textbuffer, &iter, _("Important Help menu items:"), -1, "bold", "bigfont", NULL);
  gtk_text_buffer_insert(textbuffer, &iter, _("\n"
"Geadow Tutorial	Learn-by-doing tutorial for using Emacs efficiently.\n"
"Geadow FAQ		Frequently asked questions and answers\n"
"(Non)Warranty		Geadow comes with ABSOLUTELY NO WARRANTY\n"
"Copying Conditions	Conditions for redistributing and changing Geadow.\n"
"Getting New Versions	How to obtain the latest version of Geadow.\n"
"Ordering Manuals	How to order manuals from the Geadow team.\n"), -1);
  gtk_text_view_set_cursor_visible(textview, FALSE);
  gtk_text_view_set_editable(textview, FALSE);
  gtk_text_buffer_set_modified(textbuffer, FALSE);

  g_signal_connect(textview, "button_press_event",
		   G_CALLBACK(gnu_startup_end), NULL);
  gnu_startup_timeout_handler_id = gtk_timeout_add(20000,
						   (GtkFunction)gnu_startup_end,
						   NULL);
}

int main(int argc, char** argv) {
  poptContext ctx = NULL;
  GtkWidget* geadow_app = NULL;
  GtkWidget* buffer_textview;
  GtkTextBuffer* textbuffer;

  gtk_set_locale();
  bindtextdomain(PACKAGE, GEADOW_LOCALEDIR);
  bind_textdomain_codeset(PACKAGE, "UTF-8");
  textdomain(PACKAGE);

  program = gnome_program_init(PACKAGE, VERSION,
		LIBGNOMEUI_MODULE, argc, argv,
		GNOME_PARAM_APP_DATADIR, GEADOW_DATADIR, NULL);


  glade_init();

  xml = glade_xml_new(GEADOW_GLADEDIR "/geadow.glade", NULL, PACKAGE);

  if(!xml) {
    g_error(".glade file missing %s", GEADOW_GLADEDIR "/geadow.glade");
    return 1;
  }
  glade_xml_signal_autoconnect(xml);

  geadow_app = glade_xml_get_widget(xml, "geadow_app");

  /* Create Gnome Menu */
  gnome_app_create_menus(GNOME_APP(geadow_app), menubar_uiinfo);
  gnome_app_install_menu_hints(GNOME_APP(geadow_app), menubar_uiinfo);

  /* Add buffer menus */
  /* This should be after gnome_app_create_menus(). */
  /* Otherwise, menubar_uiinfo[].widget is NULL */
  buffer_menu = gtk_menu_new();
  gtk_menu_item_set_submenu(GTK_MENU_ITEM(menubar_uiinfo[3].widget), buffer_menu); 

  geadow_info = g_new0(GeadowInfo, 1);
  geadow_info->current_view = geadow_view_new();
  geadow_info->scratch_buffer = geadow_buffer_new(); /* scratch */
  geadow_buffer_set_buffername(geadow_info->scratch_buffer, "*scratch*");
  geadow_info->current_buffer = geadow_info->scratch_buffer;
  geadow_view_set_buffer(geadow_info->current_view,
			 geadow_info->current_buffer);
  geadow_info->geadow_view_slist =
    g_slist_append(geadow_info->geadow_view_slist,
		   geadow_info->current_view);
  geadow_info->geadow_buffer_slist =
    g_slist_append(geadow_info->geadow_buffer_slist,
		   geadow_info->current_buffer);

  g_object_get(G_OBJECT(program), GNOME_PARAM_POPT_CONTEXT, &ctx, NULL);
  if(ctx) {
    char const **startup_files = poptGetArgs(ctx);
    if( startup_files != NULL )
      geadow_open_file((gchar*)*startup_files);
  }

  /* libglade-2.0 ignores widget sensitive option - it's a bug */  
  gtk_widget_set_sensitive(edit_menu_uiinfo[0].widget, FALSE); /* Undo */
  gtk_widget_set_sensitive(edit_menu_uiinfo[1].widget, FALSE); /* Redo */

  {
    GtkWidget* geadow_app_vbox = glade_xml_get_widget(xml, "geadow_app_vbox");
    GtkWidget* vbox = geadow_info->current_view->vbox;
    gtk_box_pack_start(GTK_BOX(geadow_app_vbox), vbox, TRUE, TRUE, 0);
    gtk_box_reorder_child(GTK_BOX(geadow_app_vbox), vbox,  1);
    gtk_widget_show_all(vbox);
  }

  if( geadow_info->current_buffer == geadow_info->scratch_buffer )
    gnu_startup();

  geadow_view_grab_focus(geadow_info->current_view);

  gtk_main();

  g_object_unref(G_OBJECT(xml));

  return 0;
}

GeadowBuffer*
geadow_info_get_current_buffer() {
  return geadow_info->current_buffer;
}

GeadowView*
geadow_info_get_current_view() {
  return geadow_info->current_view;
}

void
geadow_info_set_current_buffer(GeadowBuffer* buffer) {
  g_return_if_fail(buffer != NULL);

  geadow_view_set_buffer(geadow_info->current_view, buffer);
  geadow_info->current_buffer = buffer;
  geadow_view_grab_focus(geadow_info->current_view);
}
