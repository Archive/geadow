#ifndef _GEADOW_BUFFER_H
#define _GEADOW_BUFFER_H

#include <gnome.h>

typedef struct _GeadowBuffer {
  GtkTextBuffer* textbuffer;
  gchar* filename;
  gchar* buffername;
} GeadowBuffer;

GeadowBuffer* geadow_buffer_new();
GeadowBuffer* geadow_buffer_new_with_textbuffer(GtkTextBuffer* textbuffer);
void geadow_buffer_open_file(GeadowBuffer* buffer, gchar* filepath);
void geadow_buffer_save_file(GeadowBuffer* buffer);

#endif /* _GEADOW_BUFFER_H */
