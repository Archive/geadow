#include "minibuf.h"

gchar* geadow_minibuf_get_modetext();

gboolean
on_minibuf_textview_button_press_event(GtkWidget* widget) {
  return TRUE;
}

gboolean
on_minibuf_textview_key_press_event(GtkWidget* widget, GdkEventKey* event, gpointer user_data) {
  GtkWidget* buffer_textview = glade_xml_get_widget(xml, "buffer_textview");
  GtkWidget* minibuf_textview = glade_xml_get_widget(xml, "minibuf_textview");
  GtkTextView* textview = GTK_TEXT_VIEW(minibuf_textview);
  GtkTextBuffer* textbuffer = gtk_text_view_get_buffer(textview);
  GtkTextIter iter;

  /* C-g */
  if( event->state & GDK_CONTROL_MASK && event->keyval == GDK_g ) {
    geadow_minibuf_clear();
    gtk_widget_grab_focus(buffer_textview);
    return TRUE;
  }

  /* Ignore function keys */
  switch(event->keyval) {
  case GDK_Delete:
  case GDK_Home:
  case GDK_Up:
  case GDK_Down:
  case GDK_Left:
  case GDK_Right:
  case GDK_Page_Up:
  case GDK_Page_Down:
    return TRUE;
    break;
  case GDK_BackSpace:
    gtk_text_buffer_get_iter_at_mark(textbuffer, &iter,
				     gtk_text_buffer_get_insert(textbuffer));
    if( gtk_text_iter_get_line_offset(&iter) <= strlen(geadow_minibuf_get_modetext())) {
      return TRUE;
    }
    return FALSE;
    break;
  default:
    break;
  }

  return FALSE;
}

void
geadow_minibuf_clear() {
  GtkWidget* minibuf_textview = glade_xml_get_widget(xml, "minibuf_textview");
  GtkTextView* textview = GTK_TEXT_VIEW(minibuf_textview);
  GtkTextBuffer* textbuffer = gtk_text_view_get_buffer(textview);
  gchar* modetext;

  modetext = g_object_get_data(G_OBJECT(textbuffer), "modetext");
  if( modetext != NULL ) {
    g_free(modetext);
    g_object_set_data(G_OBJECT(textbuffer), "modetext", NULL);
  }

  geadow_text_buffer_clear(textbuffer);
}

void
geadow_minibuf_set_modetext(gchar* modetext) {
  GtkWidget* minibuf_textview = glade_xml_get_widget(xml, "minibuf_textview");
  GtkTextView* textview = GTK_TEXT_VIEW(minibuf_textview);
  GtkTextBuffer* textbuffer = gtk_text_view_get_buffer(textview);

  g_return_if_fail(modetext != NULL);
  g_return_if_fail(*modetext != '\0');

  geadow_text_buffer_clear(textbuffer);
  gtk_text_buffer_insert_at_cursor(textbuffer, modetext, -1);
  g_object_set_data(G_OBJECT(textbuffer), "modetext", g_strdup(modetext));
  gtk_widget_grab_focus(minibuf_textview);
}

gchar*
geadow_minibuf_get_modetext() {
  GtkWidget* minibuf_textview = glade_xml_get_widget(xml, "minibuf_textview");
  GtkTextView* textview = GTK_TEXT_VIEW(minibuf_textview);
  GtkTextBuffer* textbuffer = gtk_text_view_get_buffer(textview);

  return g_object_get_data(G_OBJECT(textbuffer), "modetext");
}
