#!/bin/sh

aclocal
glib-gettextize --force --copy
aclocal
intltoolize --force
automake -a -c
autoconf

./configure $@
