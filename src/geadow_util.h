#ifndef _GEADOW_UTIL_H
#define _GEADOW_UTIL_H

#include <gnome.h>

void geadow_text_buffer_clear(GtkTextBuffer* buffer);

#endif /* _GEADOW_UTIL_H */
