#include <stdio.h>

#include "geadow_buffer.h"
#include "geadow_info.h"

extern GtkWidget* buffer_menu;

static void
on_buffer_menuitem_activate(GtkMenuItem* menuitem, gpointer user_data) {
  GeadowBuffer* buffer = (GeadowBuffer*)user_data;

  g_assert(buffer != NULL);
  geadow_info_set_current_buffer(buffer);
  g_print("activate: %s\n", buffer->buffername);
}

GeadowBuffer*
geadow_buffer_new() {
  return geadow_buffer_new_with_textbuffer(gtk_text_buffer_new(NULL));
}

GeadowBuffer*
geadow_buffer_new_with_textbuffer(GtkTextBuffer* textbuffer) {
  GeadowBuffer* result;
  GtkWidget* menuitem;

  g_return_val_if_fail(textbuffer != NULL, NULL);
  g_return_val_if_fail(GTK_IS_TEXT_BUFFER(textbuffer), NULL);

  result = g_new0(GeadowBuffer, 1);
  result->textbuffer = textbuffer;

  /* Menu addition */
  menuitem = gtk_menu_item_new_with_label("");
  g_assert(buffer_menu != NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(buffer_menu), menuitem);
  g_signal_connect(G_OBJECT(menuitem), "activate",
		   G_CALLBACK(on_buffer_menuitem_activate), result);
  gtk_widget_show_all(menuitem);
  g_object_set_data(G_OBJECT(textbuffer), "menuitem", menuitem);

  return result;
}

void
geadow_buffer_set_buffername(GeadowBuffer* buffer, gchar* buffername) {
  GtkWidget* menuitem;

  g_return_if_fail(buffer != NULL);
  g_return_if_fail(buffername != NULL);

  if( buffer->buffername )
	g_free(buffer->buffername);

  buffer->buffername = g_strdup(buffername);
  
  menuitem = g_object_get_data(G_OBJECT(buffer->textbuffer), "menuitem");

  if( GTK_IS_LABEL(GTK_BIN(menuitem)->child) ) {
    gtk_label_set_text(GTK_LABEL(GTK_BIN(menuitem)->child), buffername);
  } else {
    g_print("No way to change menuitem label.\n");
  }
}

void
geadow_buffer_open_file(GeadowBuffer* buffer, gchar* filepath) {
  GtkTextIter start_iter;
  GtkTextIter end_iter;
  gchar* basename;

  FILE* fp;
  gchar readbuf[5120];

  g_return_if_fail(filepath != NULL);
  g_return_if_fail(*filepath != '\0');
  g_return_if_fail(buffer->textbuffer != NULL);
  g_return_if_fail(GTK_IS_TEXT_BUFFER(buffer->textbuffer));

  fp = fopen(filepath, "r");
  if( !fp ) {
    g_warning(_("Can't open file: %s"), filepath);
    return;
  }

  /* Clear current buffer contents */
  gtk_text_buffer_get_start_iter(buffer->textbuffer, &start_iter);
  gtk_text_buffer_get_end_iter(buffer->textbuffer, &end_iter);
  gtk_text_buffer_delete(buffer->textbuffer, &start_iter, &end_iter);

  while(fgets(readbuf, 5120, fp)) {
    gtk_text_buffer_insert_at_cursor(buffer->textbuffer, readbuf, -1);
  }
  fclose(fp);

  if( buffer->filename ) {
    g_free(buffer->filename);
  }
  buffer->filename = g_strdup(filepath);
  basename = g_path_get_basename(filepath);
  geadow_buffer_set_buffername(buffer, basename);
  g_free(basename);

  gtk_text_buffer_get_start_iter(buffer->textbuffer, &start_iter);
  gtk_text_buffer_place_cursor(buffer->textbuffer, &start_iter);
  gtk_text_buffer_set_modified(buffer->textbuffer, FALSE);
}

void
geadow_buffer_save_file(GeadowBuffer* buffer) {
  FILE* fp;
  GtkTextBuffer* textbuffer;
  GtkTextIter start_iter;
  GtkTextIter end_iter;
  gchar* content_text;

  g_return_if_fail(buffer != NULL);
  g_return_if_fail(buffer->filename != NULL);

  textbuffer = buffer->textbuffer;

  fp = fopen(buffer->filename, "w");
  if( !fp ) {
    g_warning(_("Can't write file: %s"), buffer->filename);
    return;
  }
  
  gtk_text_buffer_get_start_iter(textbuffer, &start_iter);
  gtk_text_buffer_get_end_iter(textbuffer, &end_iter);
  content_text = gtk_text_buffer_get_text(textbuffer,
					  &start_iter, &end_iter, TRUE);

  fputs(content_text, fp);
  fclose(fp);
  gtk_text_buffer_set_modified(textbuffer, FALSE);
}
