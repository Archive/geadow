#ifndef _GEADOW_VIEW_H
#define _GEADOW_VIEW_H

#include <gnome.h>
#include "geadow_buffer.h"

typedef struct _GeadowView {
  GtkTextView* textview;
  GtkWidget* modelabel;
  GtkWidget* scrolledwindow;
  GtkWidget* vbox;
  GeadowBuffer* buffer;
} GeadowView;

GeadowView* geadow_view_new();
void geadow_view_set_buffer(GeadowView* view, GeadowBuffer* buffer);
void geadow_view_update_modelabel(GeadowView* view);
void geadow_view_grab_focus(GeadowView* view);
void geadow_view_cut_clipboard(GeadowView* view);
void geadow_view_copy_clipboard(GeadowView* view);
void geadow_view_paste_clipboard(GeadowView* view);
void geadow_view_delete_selection(GeadowView* view);


#endif /* _GEADOW_VIEW_H */
